# DuckDuckGo Command Line Search

A simple python script to extract short answers to simple queries using the [DuckDuckGo Instant Answer API](https://api.duckduckgo.com/api).

## How to run

Requires python to run. 

```bash
python main.py
```