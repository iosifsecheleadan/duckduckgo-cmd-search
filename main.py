import traceback
from random import choice
import requests


class DuckDuckGoSearcher:
    def __init__(self):
        self.help_text = """
Ask me anything, or type something and I'll do my best to tell you about that stuff.
Type "help" and I'll help you out, or "bye bye" and I'll leave. 
"""
        # todo more prompts
        self.prompts = [
            # Polite
            "Go ahead",
            "Ask me anything",
            "What can I help you with?",
            "What would you like to know?",

            # Slightly less polite
            "Yes?",

            # Impolite
            "You again?",
            "Whatever...",
            "Riiiight...",
            "So? What'll it be?",
        ]

        # todo more waits
        self.waits = [
            "Please wait",
            "Please hold",
            "Just a sec",
            "This will be a while...",
            "Coming right up",
            "...",
            "Patience is key",
            "Hold up",

        ]

        # todo more idks
        self.idks = [
            "Sorry m8, idk...",
            "Well... This is embarrassing",
            "I really don't know what to say about that",
            "Maybe try something else",
            "I'm not sure if that's quite right",
            "Hmm... About that...",
            "Please try again",
        ]

        self.api_string_start = "https://api.duckduckgo.com/?q="
        self.api_string_end = "&format=json"

    def run(self):
        self.print_help()
        while True:
            self.print_random_prompt()
            self.run_command(input("> "))

    def run_command(self, user_input):
        if user_input == "bye bye": quit(0)
        if user_input == "help":
            self.print_help()
            return

        self.print_random_wait()
        self.print_answer("+".join(user_input.split()))

    def print_answer(self, search_string):
        try:
            response = requests.get(self.api_string_start + search_string + self.api_string_end).json()
            abstract = response["Abstract"]

            # if there is an abstract, print it
            if abstract:
                print(abstract)
                return

            # print the text of the first topic that has a text
            for element in response["RelatedTopics"]:
                text = element["Text"]
                if text:
                    print(text)
                    return

            # otherwise, print whatever
            self.print_random_idk()
        except:
            # todo custom
            traceback.print_exc()

    def print_help(self):
        print(self.help_text)

    def print_random_prompt(self):
        print()
        print(choice(self.prompts))

    def print_random_wait(self):
        print(choice(self.waits))
        print()

    def print_random_idk(self):
        print(choice(self.idks))


DuckDuckGoSearcher().run()
